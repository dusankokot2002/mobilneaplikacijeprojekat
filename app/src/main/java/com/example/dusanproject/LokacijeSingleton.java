package com.example.dusanproject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LokacijeSingleton {
    private static LokacijeSingleton instance;
    private Map<String, String> lokacijeDeskripcijeMap;

    private LokacijeSingleton() {
        lokacijeDeskripcijeMap = new HashMap<>();
    }

    public static LokacijeSingleton getInstance() {
        if (instance == null) {
            instance = new LokacijeSingleton();
        }
        return instance;
    }

    public void dodajLokacijuIDeskripciju(String lokacija, String deskripcija) {
        lokacijeDeskripcijeMap.put(lokacija, deskripcija);
    }

    public boolean sadrziLokaciju(String lokacija) {
        return lokacijeDeskripcijeMap.containsKey(lokacija);
    }



    public Map<String, String> getLokacijeDeskripcijeMap() {
        return lokacijeDeskripcijeMap;
    }


    public void obrisiLokaciju(String lokacija) {
        lokacijeDeskripcijeMap.remove(lokacija);
    }

}