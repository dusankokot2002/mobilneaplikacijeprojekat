package com.example.dusanproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

public class BusStanice extends AppCompatActivity {

    private Map<String, String> lokacijeDeskripcijeMap;

    private String[] lokacije = {
            "1.KLISA-CENTAR-LIMAN",
            "2.CENTAR-NOVO NASELJE",
            "3.PETROVARADIN-CENTAR-DETELINARA",
            "4.LIMAN IV-CENTAR-Z.STANICA",
            "5.TEMERINSKI PUT-CENTAR-AV.NASELJE",
            "6.PODBARA-CENTAR-ADICE",
            "7.NOVO NASELJE-Z.STANICA-LIMAN IV",
            "8.NOVO NASELJE-CENTAR-LIMAN I",
            "9.NOVO NASELJE-LIMAN-PETROVARADIN",
            "10.CENTAR-IND.ZONA JUG",
            "11.Z.STANICA-BOLNICA-LIMAN",
            "12.CENTAR-TELEP",
            "13.DETELINARA-LIMAN-UNIVERZITET",
            "14.CENTAR-SAJLOVO",
            "15.CENTAR-IND.ZONA SEVER",
            "16.Z.STANICA-PRISTANISNA ZONA",
            "17.BIG TC - CENTAR",
            "18.N.NASELJE-DETELINARA-CENTAR-LIMAN",
            "19.Z.STANICA-MISELUK",
            "20.Z.STANICA-LESNINA",
            "21.SANGAJ",
            "22.KAC",
            "23.BUDISAVA",
            "24.KOVILJ",
            "25.Z.STANICA-ZONA SEVER IV",
            "30.PEJICEVI SALASI",
            "31.BACKI JARAK",
            "32.TEMERIN",
            "33.GOSPODJINCI",
            "35.CENEJ",
            "41.RUMENKA",
            "42.KISAC",
            "43.STEPANOVICEVO",
            "52.VETERNIK",
            "53.FUTOG STARI",
            "54.FUTOG GRMECKA",
            "55.FUTOG BRACE BOSNJAK",
            "56. BEGEC",
            "60.S.KARLOVCI-BELILO II",
            "61.S.KARLOVCI-VINOGRADARSKA",
            "62.S.KARLOVCI-DUDARA",
            "63.CORTANOVCI",
            "64.BUKOVAC",
            "68.SR.KAMENICA - VOJINOVO",
            "69.SR.KAMENICA - CARDAK",
            "71.SR.KAMENICA- BOCKE",
            "72.PARAGOVO",
            "73.MOSINA VILA",
            "74.POPOVICA",
            "76.S.LEDINCI",
            "77.STARI RAKOVAC",
            "78.BEOCIN SELO",
            "79. CEREVIC",
            "80.B.A.S.",
            "81.BANOSTOR",
            "84.LUG",
            "86.VRDNIK"




    };





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_stanice);

        lokacijeDeskripcijeMap = new HashMap<>();
        lokacijeDeskripcijeMap.put("1.KLISA-CENTAR-LIMAN", "Smer A: KLISA - CENTAR - LIMAN I,\n" +
                "0430\n" +
                "0500 18 39GL 51\n" +
                "0601 12NRG 22 33J 43 54\n" +
                "0704NRG 15J 25 35GL 53\n" +
                "0811NRJ 30 48NRG\n" +
                "0907J 25 43NRG\n" +
                "1002 20NRG 39 57NRG\n" +
                "1115 34NRJ 52\n" +
                "1211GL 29 47NRG 57\n" +
                "1308GL 18 29NRG 39J 50\n" +
                "1400GL 11 21NRJ 31GL 42 52NRG\n" +
                "1503 13J 24 34GL 52\n" +
                "1610NRJ 29 47NRG\n" +
                "1706 24NRJ 47NRG\n" +
                "1810 33NRG 56NRJ\n" +
                "1919NRG 42\n" +
                "2005NRJ 28NRG 51\n" +
                "2114NRG 37\n" +
                "2200NRG 23NRG 46\n" +
                "2310NRG 35NRG\n" +
                "0000,\n" +
                "Smer B: LIMAN I - CENTAR - KLISA,0430\n" +
                "0500NRG 23 39J 51\n" +
                "0603 15NRG 27J 38 48GL 59\n" +
                "0709NRJ 20GL 30GL 44\n" +
                "0802NRG 21J 39 57NRG\n" +
                "0916 34NRG 53\n" +
                "1011NRG 29 48NRJ\n" +
                "1106 25GL 43\n" +
                "1201NRG 20 38NRG 52J\n" +
                "1303 13GL 24 34NRJ 44GL 55\n" +
                "1405NRG 16 26J 37 47GL 58\n" +
                "1508NRJ 18GL 29GL 43\n" +
                "1601NRG 20 38NRJ 56NRG\n" +
                "1715 33NRG 52\n" +
                "1810NRJ 33NRG 56\n" +
                "1919NRJ 42NRG\n" +
                "2005 28NRG 51\n" +
                "2114NRG 37NRG\n" +
                "2200 23NRG 46NRG\n" +
                "2310 35NRG\n" +
                "0000");
        lokacijeDeskripcijeMap.put("2.CENTAR-NOVO NASELJE", "Smer A: CENTAR - NOVO NASELJE,\n" +
                "0430\n" +
                "0500 19 38 57\n" +
                "0616 31 46\n" +
                "0701 16 31 46\n" +
                "0801 20 39 58\n" +
                "0917 36 55\n" +
                "1014 33 52\n" +
                "1111 30 49\n" +
                "1208 27 46\n" +
                "1301 16 31 46\n" +
                "1401 16 31 46\n" +
                "1501 16 31 46\n" +
                "1604 23 42\n" +
                "1705 28 51\n" +
                "1814 37\n" +
                "1900 23 46\n" +
                "2009 32 55\n" +
                "2118 41\n" +
                "2204 27 50\n" +
                "2313 36\n" +
                "0000,\n" +"Smer B: NOVO NASELJE - CENTAR \n" +
                "0430\n" +
                "0500 19 38 53\n" +
                "0608 23 46\n" +
                "0701 16 31 46\n" +
                "0801 20 39 58\n" +
                "0917 36 55\n" +
                "1014 33 52\n" +
                "1111 30 49\n" +
                "1208 27 46\n" +
                "1301 16 31 46\n" +
                "1401 16 31 46\n" +
                "1501 16 31 46\n" +
                "1604 23 42\n" +
                "1705 28 51\n" +
                "1814 37\n" +
                "1900 23 46\n" +
                "2009 32 55\n" +
                "2118 41\n" +
                "2204 27 50\n" +
                "2313 36\n" );



        lokacijeDeskripcijeMap.put("3.PETROVARADIN-CENTAR-DETELINARA", "Smer A: PETROVARADIN - CENTAR - DETELINARA "+
                       " 0430\n" + "0430\n" +
                "0500 20 40 54\n" +
                "0607 20 34 46 58\n" +
                "0710 21 32 43 54\n" +
                "0806 22 38 54\n" +
                "0910 26 42 58\n" +
                "1014 30 46\n" +
                "1102 18 34 50\n" +
                "1206 22 38 50\n" +
                "1302 14 25 36 47 58\n" +
                "1410 22 34 45 56\n" +
                "1507 18 30 44\n" +
                "1600 16 32 48\n" +
                "1704 20 36 58\n" +
                "1823 48\n" +
                "1913 38\n" +
                "2003 28 53\n" +
                "2118 43\n" +
                "2208 33 58\n" +
                "2323\n" +
                "0000\n" + "Smer B: DETELINARA - CENTAR - PETROVARADIN" + "0430\n" +
                "0500 20 40 54\n" +
                "0606 18 30 41 52\n" +
                "0703 14 26 38 50\n" +
                "0801 14 30 46\n" +
                "0902 18 34 50\n" +
                "1006 22 38 54\n" +
                "1110 26 42 58\n" +
                "1214 30 45 56\n" +
                "1307 18 30 42 54\n" +
                "1405 16 27 38 50\n" +
                "1502 14 25 36 52\n" +
                "1608 24 40 56\n" +
                "1716 36 56\n" +
                "1816 36\n" +
                "1901 26 51\n" +
                "2016 41\n" +
                "2106 31 56\n" +
                "2221 46\n" +
                "2311 36\n" +
                "0000");


        lokacijeDeskripcijeMap.put("4.LIMAN IV-CENTAR-Z.STANICA", "Smer A: LIMAN IV - CENTAR - Z.STANICA\n "+ "0430\n" +
                "0500 20 40 56\n" +
                "0611 27 42 58\n" +
                "0713 29 44\n" +
                "0800 15 31 46\n" +
                "0902 17 33 48\n" +
                "1004 19 35 50\n" +
                "1106 21 37 52\n" +
                "1208 23 39 54\n" +
                "1310 25 41 56\n" +
                "1412 27 43 58\n" +
                "1514 29 45\n" +
                "1600 16 31 47\n" +
                "1702 21 40 59\n" +
                "1818 37 56\n" +
                "1915 34 53\n" +
                "2012 31 50\n" +
                "2109 37\n" +
                "2205 33\n" +
                "2301 20 40\n" +
                "0000 \n"+ "Smer B: Z.STANICA - CENTAR - LIMAN IV \n" + "0430\n" +
                "0500 20 40 56\n" +
                "0611 27 42 58\n" +
                "0713 29 44\n" +
                "0800 15 31 46\n" +
                "0902 17 33 48\n" +
                "1004 19 35 50\n" +
                "1106 21 37 52\n" +
                "1208 23 39 54\n" +
                "1310 25 41 56\n" +
                "1412 27 43 58\n" +
                "1514 29 45\n" +
                "1600 16 31 47\n" +
                "1702 18 33 49\n" +
                "1808 27 46\n" +
                "1905 24 43\n" +
                "2002 21 41\n" +
                "2109 37\n" +
                "2205 33 52\n" +
                "2311 30\n" +
                "0000 ");
        lokacijeDeskripcijeMap.put("5.TEMERINSKI PUT-CENTAR-AV.NASELJE","Smer A: TEMERINSKI PUT - CENTAR - AVIJATIČAR.NASELJE\n" + "0430\n" +
                "0500 20 40 51\n" +
                "0602 12 22 33 44 54\n" +
                "0704 15 26 36 46 57\n" +
                "0810 26 43\n" +
                "0900 17 34 50\n" +
                "1007 24 41 58\n" +
                "1114 31 48\n" +
                "1205 22 38 55\n" +
                "1312 24 35 46 56\n" +
                "1406 17 28 38 48 59\n" +
                "1510 20 30 41 58\n" +
                "1615 32 48\n" +
                "1705 22 39 56\n" +
                "1812 33 59\n" +
                "1925 51\n" +
                "2017 43\n" +
                "2109 35\n" +
                "2201 27 53\n" +
                "2321\n" +
                "0000 \n" +"Smer B: AVIJATIČAR.NASELJE - CENTAR - TEMERINSKI PUT\n" + "0430\n" +
                "0500 20 40 51\n" +
                "0602 12 22 33 44 54\n" +
                "0704 15 26 36 46 57\n" +
                "0808 18 35 52\n" +
                "0908 25 42 59\n" +
                "1016 32 49\n" +
                "1106 23 40 56\n" +
                "1213 30 47\n" +
                "1304 14 24 35 46 56\n" +
                "1406 17 28 38 48 59\n" +
                "1510 20 35 50\n" +
                "1606 23 40 57\n" +
                "1714 32 52\n" +
                "1812 32 52\n" +
                "1912 38\n" +
                "2004 30 56\n" +
                "2122 48\n" +
                "2214 40\n" +
                "2306 32\n" +
                "0000 ");
        lokacijeDeskripcijeMap.put("6.PODBARA-CENTAR-ADICE", "Smer A: PODBARA - CENTAR - ADICE \n" + "0430\n" +
                "0510 37 57\n" +
                "0610 23 37 50\n" +
                "0703 17 30 43 57\n" +
                "0810 23 42\n" +
                "0901 20 39 58\n" +
                "1017 36 55\n" +
                "1114 33 52\n" +
                "1211 30 44 57\n" +
                "1310 24 37 50\n" +
                "1404D 17 30 44 57\n" +
                "1510D 24 37 56\n" +
                "1615 34 53\n" +
                "1712 31 50\n" +
                "1809 34 59\n" +
                "1924 49\n" +
                "2014 39\n" +
                "2104 29 54\n" +
                "2219 44\n" +
                "2309 34\n" +
                "0000\n" + "0430\n" +
                "0510 37 57\n" +
                "0610 23 37 50\n" +
                "0703 17 30 43 57\n" +
                "0810 23 42\n" +
                "0901 20 39 58\n" +
                "1017 36 55\n" +
                "1114 33 52\n" +
                "1211 30 44 57\n" +
                "1310 24 37 50\n" +
                "1404D 17 30 44 57\n" +
                "1510D 24 37 56\n" +
                "1615 34 53\n" +
                "1712 31 50\n" +
                "1809 34 59\n" +
                "1924 49\n" +
                "2014 39\n" +
                "2104 29 54\n" +
                "2219 44\n" +
                "2309 34\n" +
                "0000\n" + "Smer B: ADICE - CENTAR - PODBARA\n" + "0430\n" +
                "0500 30NRD 50\n" +
                "0610 23NRD 37 50\n" +
                "0703 10NĐ 17 30 43 57\n" +
                "0810 23 42\n" +
                "0901 20 39 58\n" +
                "1017 36 55\n" +
                "1114 33 52\n" +
                "1211 30 44 57\n" +
                "1308 10NĐ 24D 37 50\n" +
                "1404 17 30D 44 57\n" +
                "1510 24 37 56\n" +
                "1615 34 53\n" +
                "1712 31 50\n" +
                "1809 28 47\n" +
                "1912 37\n" +
                "2002 27 52\n" +
                "2117 42\n" +
                "2207 32 57\n" +
                "2325\n" +
                "0000");
        lokacijeDeskripcijeMap.put("7.NOVO NASELJE-Z.STANICA-LIMAN IV", "Smer A: NOVO NASELJE-Z.STANICA-LIMAN IV\n" + "0430\n" +
                "0510 37 57\n" +
                "0610 23 37 50\n" +
                "0703 17 30 43 57\n" +
                "0810 23 42\n" +
                "0901 20 39 58\n" +
                "1017 36 55\n" +
                "1114 33 52\n" +
                "1211 30 44 57\n" +
                "1310 24 37 50\n" +
                "1404D 17 30 44 57\n" +
                "1510D 24 37 56\n" +
                "1615 34 53\n" +
                "1712 31 50\n" +
                "1809 34 59\n" +
                "1924 49\n" +
                "2014 39\n" +
                "2104 29 54\n" +
                "2219 44\n" +
                "2309 34\n" +
                "0000\n" + "Smer B:LIMAN IV-Z.STANICA-NOVO NASELJE\n" + "0430\n" +
                "0500 30NRD 50\n" +
                "0610 23NRD 37 50\n" +
                "0703 10NĐ 17 30 43 57\n" +
                "0810 23 42\n" +
                "0901 20 39 58\n" +
                "1017 36 55\n" +
                "1114 33 52\n" +
                "1211 30 44 57\n" +
                "1308 10NĐ 24D 37 50\n" +
                "1404 17 30D 44 57\n" +
                "1510 24 37 56\n" +
                "1615 34 53\n" +
                "1712 31 50\n" +
                "1809 28 47\n" +
                "1912 37\n" +
                "2002 27 52\n" +
                "2117 42\n" +
                "2207 32 57\n" +
                "2325\n" +
                "0000");
        lokacijeDeskripcijeMap.put("8.NOVO NASELJE-CENTAR-LIMAN I","Smer A: NOVO NASELJE - CENTAR - LIMAN I\n" + "0430\n" +
                "0500 21 42 57\n" +
                "0608 18 28 38 48 58\n" +
                "0708 18 28 38 48 58\n" +
                "0808 18 30 45\n" +
                "0900 15 30 45\n" +
                "1000 15 30 45\n" +
                "1100 15 30 45\n" +
                "1200 15 30 45\n" +
                "1300 10 20 30 40 50\n" +
                "1400 10 20 30 40 50\n" +
                "1500 10 20 30 45\n" +
                "1600 15 30 45\n" +
                "1700 15 30 45\n" +
                "1800 15 30 45\n" +
                "1908 30 53\n" +
                "2015 38\n" +
                "2100 23 45\n" +
                "2208 30 53\n" +
                "2315 38\n" +
                "0000\n" + "Smer B: LIMAN I - CENTAR - NOVO NASELJE\n" + "0430\n" +
                "0500 21 42 57\n" +
                "0608 19 30 42 53\n" +
                "0703 13 23 33 43 53\n" +
                "0803 13 23 33 45\n" +
                "0900 15 30 45\n" +
                "1000 15 30 45\n" +
                "1100 15 30 45\n" +
                "1200 15 30 45\n" +
                "1300 15 25 35 45 55\n" +
                "1405 15 25 35 45 55\n" +
                "1505 15 25 35 45\n" +
                "1600 15 30 45\n" +
                "1700 15 30 45\n" +
                "1800 15 30 45\n" +
                "1908 30 53\n" +
                "2015 38\n" +
                "2100 23 45\n" +
                "2208 30 53\n" +
                "2315 38\n" +
                "0000");
        lokacijeDeskripcijeMap.put("9.NOVO NASELJE-LIMAN-PETROVARADIN", "Smer A: NOVO NASELJE - LIMAN - PETROVARADIN\n" +  "0430\n" +
                "0500ALI 25 43ALI 57ALI\n" +
                "0612 27ALI 42 56ALI\n" +
                "0709 22 35ALI 48\n" +
                "0801ALI 14 32 50ALI\n" +
                "0908 26ALI 44\n" +
                "1002ALI 20 38ALI 56ALI\n" +
                "1114 32 50ALI\n" +
                "1208ALI 26 44ALI\n" +
                "1302 17ALI 32 47ALI\n" +
                "1401 16ALI 29 42ALI 55\n" +
                "1508ALI 22ALI 40 58ALI\n" +
                "1616 34 52ALI\n" +
                "1710 28 46ALI\n" +
                "1804 22ALI 40 58\n" +
                "1916ALI 34 52ALI\n" +
                "2010 28ALI 49\n" +
                "2114 39ALI\n" +
                "2204 29 54\n" +
                "2327\n" +
                "0000\n" + "0430\n" +
                "0500 25 43ALI 57ALI\n" +
                "0610 23ALI 36 49ALI\n" +
                "0702ALI 15 28ALI 41 56ALI\n" +
                "0814 32 50ALI\n" +
                "0908 26 44ALI\n" +
                "1002 20ALI 38 56ALI\n" +
                "1114 32ALI 50ALI\n" +
                "1208 26 44ALI\n" +
                "1302ALI 17 30ALI 43 56ALI\n" +
                "1409 22ALI 35 48ALI\n" +
                "1501 14ALI 27 40ALI 58\n" +
                "1616ALI 34 52ALI\n" +
                "1710 28 46ALI\n" +
                "1804 22 40ALI 58\n" +
                "1916ALI 34 52\n" +
                "2011ALI 32 53ALI\n" +
                "2114 39ALI\n" +
                "2204 37ALI\n" +
                "2318\n" +
                "0000");
        lokacijeDeskripcijeMap.put("10.CENTAR-IND.ZONA JUG", "Smer A: CENTAR - INDUST.ZONA JUG\n" + "0520AL 30APT 35MAL\n" +
                "0615AL 40MAL\n" +
                "0735MAL 40APT\n" +
                "1330APT 35AL\n" +
                "2130APT 30AL\n" + "Smer B: INDUST.ZONA JUG - CENTAR\n" + "0605AL 15APT\n" +
                "1410ALM 15APT\n" +
                "1505ALM\n" +
                "1610AL 15APT\n" +
                "2215AL 15APT");
        lokacijeDeskripcijeMap.put("11.Z.STANICA-BOLNICA-LIMAN", "Smer A: Z.STANICA - BOLNICA - LIMAN \n" + "0500 15 29 44 58\n" +
                "0613 27 42 56\n" +
                "0711 20MDJ 25 30MDJ 39 40MDJ 54\n" +
                "0813 32 51\n" +
                "0910 29 48\n" +
                "1007 26 45\n" +
                "1104 23 42\n" +
                "1201 20 39 58\n" +
                "1313 27 42 56\n" +
                "1411 25 40 54\n" +
                "1509 23 42\n" +
                "1601 20 39 58\n" +
                "1717 36 55\n" +
                "1814 40\n" +
                "1906 32 58\n" +
                "2024 50\n" +
                "2116 42\n" +
                "2208 36\n" +
                "2304 32\n" +
                "0000\n" + "Smer B: Z.STANICA - LIMAN - BOLNICA\n " + "0500 15 29 44 58\n" +
                "0613 27 42 56\n" +
                "0711 25 39 54\n" +
                "0813 32 51\n" +
                "0910 29 48\n" +
                "1007 26 45\n" +
                "1104 23 42\n" +
                "1201 20 39 58\n" +
                "1313 27 42 56\n" +
                "1400DJM 11 25 40 54\n" +
                "1509 23 42\n" +
                "1601 20 39 58\n" +
                "1717 36 55\n" +
                "1814 40\n" +
                "1906 32 58\n" +
                "2024 50\n" +
                "2116 42\n" +
                "2208 36\n" +
                "2304 32\n" +
                "0000");
        lokacijeDeskripcijeMap.put("12.CENTAR-TELEP", "Smer A: CENTAR - TELEP\n" + "0430\n" +
                "0500 17 34 51\n" +
                "0608 25 42 59\n" +
                "0716 33 50\n" +
                "0807 24 41 58\n" +
                "0915 32 49\n" +
                "1006 23 40 57\n" +
                "1114 31 48\n" +
                "1205 22 39 56\n" +
                "1313 30 47\n" +
                "1404 21 38 55\n" +
                "1512 29 46\n" +
                "1603 20 37 54\n" +
                "1711 28 50\n" +
                "1813 36 59\n" +
                "1922 45\n" +
                "2008 31 54\n" +
                "2117 40\n" +
                "2203 26 49\n" +
                "2312 35\n" +
                "0000\n" + "Smer B: TELEP - CENTAR\n" + "0430\n" +
                "0500 17 34 51\n" +
                "0608 25 42 59\n" +
                "0716 33 50\n" +
                "0807 24 41 58\n" +
                "0915 32 49\n" +
                "1006 23 40 57\n" +
                "1114 31 48\n" +
                "1205 22 39 56\n" +
                "1313 30 47\n" +
                "1404 21 38 55\n" +
                "1512 29 46\n" +
                "1603 20 37 54\n" +
                "1716 38\n" +
                "1801 24 47\n" +
                "1910 33 56\n" +
                "2019 42\n" +
                "2105 28 51\n" +
                "2214 37\n" +
                "2300 30\n" +
                "0000");
        lokacijeDeskripcijeMap.put("13.DETELINARA-LIMAN-UNIVERZITET", "Smer A: DETELINARA - LIMAN - UNIVERZITET\n" + "0430\n" +
                "0500 30 45\n" +
                "0600 15 30 45\n" +
                "0700 15 22UDJ 30 45\n" +
                "0800 20 40\n" +
                "0900 20 40\n" +
                "1000 20 40\n" +
                "1100 20 40\n" +
                "1200 20 40\n" +
                "1300 15 30 45\n" +
                "1400 15 30 45\n" +
                "1500 15 30 50\n" +
                "1610 30 50\n" +
                "1710 30 50\n" +
                "1810 30\n" +
                "1900 30\n" +
                "2000 30\n" +
                "2100 30\n" +
                "2200 30\n" +
                "2300 30\n" +
                "0000\n"+"Smer B: UNIVERZITET - LIMAN - DETELINARA\n"+ "0430\n" +
                "0500 30 45\n" +
                "0600 15 30 45\n" +
                "0700 15 30 50\n" +
                "0810 30 50\n" +
                "0910 30 50\n" +
                "1010 30 50\n" +
                "1110 30 50\n" +
                "1210 30 45\n" +
                "1300 15 30 45\n" +
                "1400 15 30 45\n" +
                "1500 15 30 45\n" +
                "1600 20 40\n" +
                "1700 20 40\n" +
                "1800 30\n" +
                "1900 30\n" +
                "2000 30\n" +
                "2100 30\n" +
                "2200 30\n" +
                "2300 30\n" +
                "0000");
        lokacijeDeskripcijeMap.put("14.CENTAR-SAJLOVO", "Smer A: CENTAR - SAJLOVO\n" + "0430\n" +
                "0500 30 47\n" +
                "0604 21 38 55\n" +
                "0712 29 46\n" +
                "0807 28 49\n" +
                "0910 31 52\n" +
                "1013 34 55\n" +
                "1116 37 58\n" +
                "1219 40\n" +
                "1301 22 41 58\n" +
                "1415 32 49\n" +
                "1506 23 40 57\n" +
                "1614 35 56\n" +
                "1717 38\n" +
                "1800 30\n" +
                "1900 30\n" +
                "2000 30\n" +
                "2100 30\n" +
                "2200 30\n" +
                "2300 30\n" +
                "0000\n" + "Smer B: SAJLOVO - CENTAR\n" + "0430\n" +
                "0500 30 47\n" +
                "0604 21 38 55\n" +
                "0714 25DDJ 30DDJ 35 56\n" +
                "0817 38 59\n" +
                "0920 41\n" +
                "1002 23 44\n" +
                "1105 26 47\n" +
                "1208 29 50\n" +
                "1307 20DDJ 24 25DDJ 41 58\n" +
                "1415 32 49\n" +
                "1506 23 42\n" +
                "1603 24 45\n" +
                "1706 27 48\n" +
                "1809 30\n" +
                "1900 30\n" +
                "2000 30\n" +
                "2100 30\n" +
                "2200 30\n" +
                "2300 30\n" +
                "0000");
        lokacijeDeskripcijeMap.put("15.CENTAR-IND.ZONA SEVER", "Smer A: CENTAR - INDUSTR.ZONA SEVER\n" + "0520NK 30NK 30APT\n" +
                "0605NP 35NP\n" +
                "0700NP 30APT 30NK\n" +
                "0930NK\n" +
                "1230NP\n" +
                "1320NK 25NK 30APT\n" +
                "1720NK\n" +
                "2130NK 30APT\n" + "Smer B: INDUSTR.ZONA SEVER - CENTAR\n" + "0615KN 15APT\n" +
                "0730NP\n" +
                "1005KN\n" +
                "1320NP\n" +
                "1415KN 15APT\n" +
                "1510NP\n" +
                "1610KN 15APT\n" +
                "1815KN\n" +
                "2120NP\n" +
                "2215KN 15APT" );
        lokacijeDeskripcijeMap.put("16.Z.STANICA-PRISTANISNA ZONA", "Smer A: ZELE.STANICA - PRISTANISNA ZONA\n"+  "0540\n" +
                "0645\n" +
                "0745\n" +
                "1335\n" + "Smer B: PRISTANISNA ZONA - ZELE.STANICA\n" + "0615\n" +
                "1415\n" +
                "1610");
        lokacijeDeskripcijeMap.put("17.BIG TC - CENTAR", "Smer A: BIG TC - CENTAR\n" + "0953\n" +
                "1033\n" +
                "1113 53\n" +
                "1233\n" +
                "1313 53\n" +
                "1433\n" +
                "1513 53\n" +
                "1633\n" +
                "1713 53\n" +
                "1833\n" +
                "1913 53\n" +
                "2033\n" +
                "2113 53\n" +
                "2233\n" + "Smer B: CENTAR - BIG TC\n" + "0730\n" +
                "0830\n" +
                "0930\n" +
                "1010 50\n" +
                "1130\n" +
                "1210 50\n" +
                "1330\n" +
                "1410 50\n" +
                "1530\n" +
                "1610 50\n" +
                "1730\n" +
                "1810 50\n" +
                "1930\n" +
                "2010 50\n" +
                "2130\n" +
                "2210");
        lokacijeDeskripcijeMap.put("18.N.NASELJE-DETELINARA-CENTAR-LIMAN", "Smer A: N.NASELJE - DETELINARA - CENTAR\n" + "0030\n" +
                "0130\n" +
                "0230\n" +
                "0330\n" + "Smer B: N.NASELJE - LIMAN - CENTAR - DETELINARA\n"+ "0000\n" +
                "0100\n" +
                "0200\n" +
                "0300\n" +
                "0400");
        lokacijeDeskripcijeMap.put("19.Z.STANICA-MISELUK", "Smer A: Z.STANICA - RTV - MISELUK(BOLNICA)\n" + "0530\n" +
                "0615\n" +
                "0700\n" +
                "0800 40\n" +
                "1230\n" +
                "1330\n" +
                "1815\n" +
                "2030\n" + "Smer B: MISELUK(BOLNICA) - RTV - Z.STANICA\n" + "0615\n" +
                "0705 30\n" +
                "1305\n" +
                "1415\n" +
                "1510 30\n" +
                "1615\n" +
                "1715\n" +
                "1815\n" +
                "1930\n" +
                "2300");
        lokacijeDeskripcijeMap.put("20.Z.STANICA-LESNINA", "Smer A: ZEL.STANICA - LESNINA\n" + "0730\n" +
                "0900\n" +
                "1130\n" +
                "1530\n" +
                "1700\n" +
                "1830\n" + "Smer B: LESNINA - ZEL.STANICA\n" + "0800\n" +
                "0930\n" +
                "1200\n" +
                "1600\n" +
                "1730\n" +
                "1930\n" +
                "2030");
        lokacijeDeskripcijeMap.put("21.SANGAJ", "Smer A: Polasci za ŠANGAJ\n" + "0420 55\n" +
                "0535\n" +
                "0615TET 35\n" +
                "0700 25NIS 55\n" +
                "0900 55\n" +
                "1050\n" +
                "1135\n" +
                "1230\n" +
                "1305 30\n" +
                "1405 30\n" +
                "1510 45\n" +
                "1630\n" +
                "1730\n" +
                "1845\n" +
                "1925\n" +
                "2035\n" +
                "2130\n" +
                "2245\n" + "Smer B: Polasci iz ŠANGAJ\n" + "0455\n" +
                "0520\n" +
                "0610 35\n" +
                "0700 20 50\n" +
                "0820\n" +
                "0925\n" +
                "1020\n" +
                "1110\n" +
                "1200 55\n" +
                "1330\n" +
                "1405 30TET\n" +
                "1505 35\n" +
                "1610 55\n" +
                "1805\n" +
                "1915 50\n" +
                "2100\n" +
                "2210\n" +
                "2305");
        lokacijeDeskripcijeMap.put("22.KAC", "Smer A: Polasci za KAĆ\n" + "0520\n" +
                "0610\n" +
                "0715 50\n" +
                "0900\n" +
                "1050\n" +
                "1115KC\n" +
                "1215\n" +
                "1340\n" +
                "1405KC 30\n" +
                "1510 25 50\n" +
                "1605KC 33\n" +
                "1900KC 25\n" +
                "2125\n" + "Smer B: Polasci iz KAĆ\n" +  "0505 55\n" +
                "0615 30C 45\n" +
                "0710 50\n" +
                "0825 55\n" +
                "0935 50\n" +
                "1125 50\n" +
                "1230Đ 50\n" +
                "1415 35KC\n" +
                "1505 45\n" +
                "1600 30 40KC\n" +
                "1935KC\n" +
                "2000");
        lokacijeDeskripcijeMap.put("23.BUDISAVA", "Smer A: Polasci za BUDISAVA\n" + "0440\n" +
                "0705\n" +
                "0945\n" +
                "1105 55\n" +
                "1225\n" +
                "1315\n" +
                "1435\n" +
                "1500 30\n" +
                "1705 30 55Đ\n" +
                "1825Đ\n" +
                "1925\n" +
                "2000 55\n" +
                "2330\n" + "Smer B: Polasci iz BUDISAVA\n"  + "0450\n" +
                "0510NT\n" +
                "0600C 15C 25 40 55\n" +
                "0745\n" +
                "1025\n" +
                "1145\n" +
                "1235\n" +
                "1305 50\n" +
                "1515 40\n" +
                "1610\n" +
                "1745\n" +
                "1810 35Đ\n" +
                "1905Đ\n" +
                "2005 40\n" +
                "2135NT\n" +
                "0355C");
        lokacijeDeskripcijeMap.put("24.KOVILJ", "Smer A: Polasci za KOVILJ\n" + "0400\n" +
                "0500 35\n" +
                "0640\n" +
                "0730\n" +
                "0830\n" +
                "0930\n" +
                "1030\n" +
                "1130\n" +
                "1225\n" +
                "1305 30\n" +
                "1405AKN 15 40\n" +
                "1515 35\n" +
                "1610 40\n" +
                "1715\n" +
                "1800 35\n" +
                "1935\n" +
                "2025\n" +
                "2130\n" +
                "2220 45\n" +
                "0115KSA\n" + "Smer B: Polasci iz KOVILJ\n" + "0415 45NKA 50C\n" +
                "0510 45\n" +
                "0600 30 50\n" +
                "0740\n" +
                "0830\n" +
                "0930\n" +
                "1030\n" +
                "1130\n" +
                "1230\n" +
                "1325\n" +
                "1405 30\n" +
                "1515 40\n" +
                "1615 35\n" +
                "1710 40\n" +
                "1815 55\n" +
                "1930\n" +
                "2035\n" +
                "2120\n" +
                "2220C\n" +
                "2335C\n" +
                "0245C");
        lokacijeDeskripcijeMap.put("25.Z.STANICA-ZONA SEVER IV", "Smer A: Polasci za Z.STANICA-ZONA SEVER IV \n" + "0440\n" +
                "0705\n" +
                "0945\n" +
                "1105 55\n" +
                "1225\n" +
                "1315\n" +
                "1435\n" +
                "1500 30\n" +
                "1705 30 55Đ\n" +
                "1825Đ\n" +
                "1925\n" +
                "2000 55\n" +
                "2330\n" + "Smer B: Polasci iz BUDISAVA\n" + "0450\n" +
                "0510NT\n" +
                "0600C 15C 25 40 55\n" +
                "0745\n" +
                "1025\n" +
                "1145\n" +
                "1235\n" +
                "1305 50\n" +
                "1515 40\n" +
                "1610\n" +
                "1745\n" +
                "1810 35Đ\n" +
                "1905Đ\n" +
                "2005 40\n" +
                "2135NT\n" +
                "0355C");
        lokacijeDeskripcijeMap.put("30.PEJICEVI SALASI", "Smer A: Polasci za PEJIĆEVI SALAŠI\n" + "0615\n" +
                "0930\n" +
                "1230\n" +
                "1500\n" +
                "1830\n" +
                "2030\n" + "Smer B: Polasci iz PEJIĆEVI SALAŠI\n" + "0645\n" +
                "1000\n" +
                "1300\n" +
                "1530\n" +
                "1900\n" +
                "2100");
        lokacijeDeskripcijeMap.put("31.BACKI JARAK", "Smer A: Polasci za BAČKI JARAK\n" + "0455K\n" + "Smer B: Polasci iz BAČKI JARAK\n" + "0525K\n" +
                "0635ĐK");
        lokacijeDeskripcijeMap.put( "32.TEMERIN", "Smer A: Polasci za TEMERIN\n" + "0515\n" +
                "0615 30\n" +
                "0710 50\n" +
                "0850\n" +
                "1020 40\n" +
                "1120 50\n" +
                "1210 30\n" +
                "1310\n" +
                "1405AKN 05 15 35\n" +
                "1500 30 50\n" +
                "1610 50\n" +
                "1730\n" +
                "1810 40\n" +
                "1900 20 35\n" +
                "2020\n" +
                "2215ML\n" +
                "0115\n" + "Smer B: Polasci iz TEMERIN\n" + "0450\n" +
                "0510NK 40 50\n" +
                "0605 15 30\n" +
                "0705 20 30\n" +
                "0800 40\n" +
                "0940\n" +
                "1110 30\n" +
                "1210 40\n" +
                "1300 20 45\n" +
                "1405 55\n" +
                "1505 25 50\n" +
                "1620 40\n" +
                "1700 40\n" +
                "1900 50\n" +
                "2015\n" +
                "2110 55\n" +
                "0245\n" +
                "0400");
        lokacijeDeskripcijeMap.put( "33.GOSPODJINCI", "Smer A: Polasci za GOSPOĐINCI\n" + "0410T\n" +
                "0550\n" +
                "0620\n" +
                "0735T\n" +
                "0935T\n" +
                "1100\n" +
                "1240\n" +
                "1330T\n" +
                "1430\n" +
                "1510\n" +
                "1620\n" +
                "1710\n" +
                "1825T\n" +
                "1945\n" +
                "2040T\n" +
                "2140T\n" +
                "2245T\n" + "Smer B: Polasci iz GOSPOĐINCI\n" + "0445\n" +
                "0515 45\n" +
                "0650\n" +
                "0715T\n" +
                "0835\n" +
                "1035\n" +
                "1200\n" +
                "1330T\n" +
                "1430\n" +
                "1530\n" +
                "1610\n" +
                "1720\n" +
                "1810T\n" +
                "1920DT\n" +
                "2045DT\n" +
                "2140T\n" +
                "2230DT");
        lokacijeDeskripcijeMap.put( "35.CENEJ", "Smer A: Polasci za ČENEJ\n" + "0455ČL\n" +
                "0615ČL\n" +
                "0705ČL\n" +
                "0905\n" +
                "1030ČL\n" +
                "1140ČL\n" +
                "1300ČLĐ 30\n" +
                "1430\n" +
                "1520\n" +
                "1620\n" +
                "1730\n" +
                "1840\n" +
                "1945\n" +
                "2035\n" +
                "2140\n" +
                "2245\n" + "Smer B: Polasci iz ČENEJ\n" +
                 "0450\n" +
                "0540\n" +
                "0610 35\n" +
                "0700\n" +
                "0800\n" +
                "0940\n" +
                "1115\n" +
                "1230\n" +
                "1345Đ\n" +
                "1405ČL\n" +
                "1510ČL 55ČL\n" +
                "1655ČL\n" +
                "1805ČL\n" +
                "1920ČL\n" +
                "2020ČL\n" +
                "2110ČL\n" +
                "2215ČL\n" +
                "2315ČL");
        lokacijeDeskripcijeMap.put( "41.RUMENKA", "Smer A: Polasci za RUMENKA\n" + "0605 40\n" +
                "0735\n" +
                "0820 55\n" +
                "1050\n" +
                "1155\n" +
                "1220\n" +
                "1315 30\n" +
                "1420 40\n" +
                "1520\n" +
                "1615\n" +
                "1700 25\n" +
                "1810\n" +
                "1910Đ\n" + "Smer B: Polasci iz RUMENKA\n" + "0525\n" +
                "0600 30 45\n" +
                "0710 20Đ\n" +
                "0805 45\n" +
                "0925\n" +
                "1125\n" +
                "1225 50\n" +
                "1305 45\n" +
                "1400 50\n" +
                "1505 50\n" +
                "1645\n" +
                "1730 55\n" +
                "1840\n" +
                "1940Đ\n" +
                "0300");
        lokacijeDeskripcijeMap.put("42.KISAC", "Smer A: Polasci za KISAČ\n" + "0450\n" +
                "0520\n" +
                "0610 30\n" +
                "0700\n" +
                "0910\n" +
                "1130\n" +
                "1215\n" +
                "1335\n" +
                "1410KN 30\n" +
                "1525 50\n" +
                "1745\n" +
                "1915\n" +
                "2015\n" +
                "2220KR\n" + "Smer B: Polasci iz KISAČ\n" + "0445\n" +
                "0500NK 30\n" +
                "0600 10 50\n" +
                "0710 40\n" +
                "0950\n" +
                "1210 55\n" +
                "1415\n" +
                "1510\n" +
                "1605 30\n" +
                "1825\n" +
                "1955\n" +
                "2055\n" +
                "0245");
        lokacijeDeskripcijeMap.put( "43.STEPANOVICEVO", "Smer A: Polasci za STEPANOVIĆEVO\n" + "0555\n" +
                "0650\n" +
                "0750TK\n" +
                "0935\n" +
                "1010TK\n" +
                "1115\n" +
                "1200 50TK\n" +
                "1340\n" +
                "1445TK\n" +
                "1530\n" +
                "1630TK\n" +
                "1710TK\n" +
                "1820TK\n" +
                "1940\n" +
                "2035\n" +
                "2125RTK\n" +
                "2245TK\n" +
                "0115TK\n" + "Smer B: Polasci iz STEPANOVIĆEVO\n" + "0455KR\n" +
                "0540\n" +
                "0610TK 45\n" +
                "0735\n" +
                "0845\n" +
                "1025KR\n" +
                "1105\n" +
                "1205 50\n" +
                "1345\n" +
                "1430\n" +
                "1540\n" +
                "1620\n" +
                "1725\n" +
                "1805\n" +
                "1915\n" +
                "2030\n" +
                "2125\n" +
                "2220\n" +
                "2340\n" +
                "0230");
        lokacijeDeskripcijeMap.put( "52.VETERNIK", "Smer A: Polasci za VETERNIK\n" + "0625 55\n" +
                "0730 55\n" +
                "0835\n" +
                "0950\n" +
                "1010 50\n" +
                "1105\n" +
                "1210\n" +
                "1300 30 50\n" +
                "1425 45\n" +
                "1505 33\n" +
                "1635\n" +
                "1735\n" +
                "1855\n" +
                "1915\n" +
                "2030\n" +
                "2100 40\n" +
                "2210 40\n" +
                "2345SP\n" + "Smer B: Polasci iz VETERNIK" + "0455\n" +
                "0520\n" +
                "0620 40 55\n" +
                "0710 30\n" +
                "0810 35\n" +
                "0915\n" +
                "1030 50\n" +
                "1130 45\n" +
                "1250\n" +
                "1310 40\n" +
                "1410 30\n" +
                "1505 25 45\n" +
                "1610\n" +
                "1710\n" +
                "1815\n" +
                "1930 55\n" +
                "2110 35\n" +
                "2215 50\n" +
                "2310");
        lokacijeDeskripcijeMap.put( "53.FUTOG STARI", "Smer A: Polasci za FUTOG STARI\n" + "0510\n" +
                "0620 35 50\n" +
                "0728\n" +
                "0800 35CG\n" +
                "1030\n" +
                "1105 20\n" +
                "1230 48\n" +
                "1357\n" +
                "1415 40\n" +
                "1510 30\n" +
                "1605 55\n" +
                "1805 20 57\n" +
                "1920\n" +
                "2010 50\n" +
                "2150\n" + "Smer B: Polasci iz FUTOG STARI\n" + "0400 30\n" +
                "0510 55\n" +
                "0600 25\n" +
                "0725 40\n" +
                "0820 50\n" +
                "0925CG\n" +
                "1120 55\n" +
                "1200Đ 20Đ\n" +
                "1320 40\n" +
                "1445\n" +
                "1505 30\n" +
                "1600 20\n" +
                "1745\n" +
                "1855\n" +
                "1905 40\n" +
                "2010\n" +
                "2100 40\n" +
                "2240\n" +
                "0310");
        lokacijeDeskripcijeMap.put( "54.FUTOG GRMECKA", "Smer A: Polasci za FUTOG GRMEČKA\n" + "0500ZA\n" +
                "0610ZA 53\n" +
                "0735\n" +
                "0915 40\n" +
                "1010\n" +
                "1200 25Đ\n" +
                "1300 15 20\"A\"\n" +
                "1410\"A\"\n" +
                "1535\n" +
                "1625 45\n" +
                "1735\n" + "Smer B: Polasci iz FUTOG GRMEČKA\n" + "0510 30 50AZG\n" +
                "0610 50KF\n" +
                "0700AZG 35\n" +
                "0825\n" +
                "1000 25\n" +
                "1100\n" +
                "1245\n" +
                "1310Đ 45\n" +
                "1400 10\"A\"\n" +
                "1500\"A\"\n" +
                "1715 30\n" +
                "1820");
        lokacijeDeskripcijeMap.put( "55.FUTOG BRACE BOSNJAK", "Smer A: Polasci za FUTOG BRAĆE BOŠNJAK\n" + "\n" +
                "0400 50\n" +
                "0530 40\n" +
                "0605 40\n" +
                "0710 40\n" +
                "0825 50\n" +
                "0925 50CG\n" +
                "1035CG\n" +
                "1110Đ 30 55\n" +
                "1205CG 45\n" +
                "1300 35 51\n" +
                "1405CG 15KN 20 35 52\n" +
                "1540CG 45\n" +
                "1610 30 50\n" +
                "1715 40 55\n" +
                "1825 35 55\n" +
                "1915 30 50\n" +
                "2005 40\n" +
                "2105\n" +
                "2215\n" +
                "0000\n" + "Smer B: Polasci iz FUTOG BRAĆE BOŠNJAK\n" + "0445\n" +
                "0500NK 25 35\n" +
                "0605 20 35 55\n" +
                "0710 30\n" +
                "0805 35\n" +
                "0915 40\n" +
                "1015 45CG\n" +
                "1130CG\n" +
                "1210 25 50 55CG\n" +
                "1340 50\n" +
                "1430 45 55CG\n" +
                "1515 25 40\n" +
                "1635CG 40 55\n" +
                "1705 25 45\n" +
                "1810 35 45\n" +
                "1915 25 45\n" +
                "2005 20 40\n" +
                "2100 30 55\n" +
                "2305");
        lokacijeDeskripcijeMap.put( "56. BEGEC", "Smer A: Polasci za BEGEČ\n" + "0430\n" +
                "0520 55\n" +
                "0630\n" +
                "0720\n" +
                "0810\n" +
                "0900\n" +
                "1005 55\n" +
                "1150\n" +
                "1250\n" +
                "1325\n" +
                "1400 25 55\n" +
                "1515 43\n" +
                "1620\n" +
                "1710\n" +
                "1800 50\n" +
                "1940\n" +
                "2035\n" +
                "2125\n" +
                "2230 50SF\n" +
                "2345\n" +
                "0115SF\n" + "Smer B: Polasci iz BEGEČ\n" + "0350SF\n" +
                "0435C\n" +
                "0515 45\n" +
                "0605 45\n" +
                "0720\n" +
                "0810\n" +
                "0900 50\n" +
                "1055\n" +
                "1145\n" +
                "1240\n" +
                "1340\n" +
                "1415 50\n" +
                "1520 45\n" +
                "1605 35\n" +
                "1715\n" +
                "1800 50\n" +
                "1940\n" +
                "2030\n" +
                "2125\n" +
                "2215\n" +
                "2320 40SF\n" +
                "0300");
        lokacijeDeskripcijeMap.put( "60.S.KARLOVCI-BELILO II", "Smer A: Polasci za S.KARLOVCI - BELILO II\n" + "0830\n" +
                "1305\n" +
                "1650\n" +
                "1843\n" + "Smer B: Polasci iz BELILO II - S.KARLOVCI\n" +  "0645\n" +
                "0915\n" +
                "1350\n" +
                "1735\n" +
                "1940");
        lokacijeDeskripcijeMap.put( "61.S.KARLOVCI-VINOGRADARSKA", "Smer A: Polasci za SR.KARLOVCI VINOGRADARSKA\n" + "0520\n" +
                "0610 45 55DJC\n" +
                "0750\n" +
                "0810\n" +
                "0910 30\n" +
                "1030\n" +
                "1130\n" +
                "1205 45 55\n" +
                "1330\n" +
                "1425\n" +
                "1510\n" +
                "1610\n" +
                "1715\n" +
                "1815 55\n" +
                "1940\n" +
                "2035\n" +
                "2140\n" +
                "0150\n" + "Smer B: Polasci iz SR.KARLOVCI VINOGRADARSKA\n" + "0405OC\n" +
                "0530 55CKR\n" +
                "0620 30Đ 50\n" +
                "0725 30CĐ\n" +
                "0830 50\n" +
                "0950\n" +
                "1010\n" +
                "1110\n" +
                "1210 50\n" +
                "1335 40\n" +
                "1410\n" +
                "1510\n" +
                "1655\n" +
                "1755\n" +
                "1935KD\n" +
                "2215\n" +
                "2335");
        lokacijeDeskripcijeMap.put(  "62.S.KARLOVCI-DUDARA", "Smer A: Polasci za SR.KARLOVCI DUDARA\n" + "0445\n" +
                "0530\n" +
                "0605 50\n" +
                "0730\n" +
                "0850\n" +
                "1010 50\n" +
                "1150\n" +
                "1215Đ 40\n" +
                "1320\n" +
                "1405 15 40\n" +
                "1515 30 45\n" +
                "1635KVG\n" +
                "1730 55KVG\n" +
                "1825\n" +
                "1915\n" +
                "2015\n" +
                "2100\n" +
                "2245\n" + "Smer B: Polasci iz SR.KARLOVCI DUDARA\n" + "0445KVG\n" +
                "0520\n" +
                "0610 50\n" +
                "0730\n" +
                "0810\n" +
                "0930\n" +
                "1050\n" +
                "1130\n" +
                "1235 55Đ\n" +
                "1330\n" +
                "1405 45\n" +
                "1500 30\n" +
                "1600KVG 10 30\n" +
                "1720KVG\n" +
                "1810 40KVG\n" +
                "1905 55\n" +
                "2055KVG\n" +
                "2140\n" +
                "2325");
        lokacijeDeskripcijeMap.put( "63.CORTANOVCI", "Smer A: Polasci za ČORTANOVCI\n" + "0500 35Đ\n" +
                "1115\n" +
                "1446\n" +
                "1546\n" +
                "1630\n" +
                "1925Đ\n" +
                "2155\n" + "Smer B: Polasci iz ČORTANOVCI\n" + "0500 55\n" +
                "0625Đ\n" +
                "1210\n" +
                "1541\n" +
                "1649\n" +
                "1725\n" +
                "2025Đ");
        lokacijeDeskripcijeMap.put( "64.BUKOVAC", "Smer A: Polasci za BUKOVAC\n" + "0515 30\n" +
                "0605 30 55\n" +
                "0715 50\n" +
                "0910\n" +
                "1030\n" +
                "1110 55\n" +
                "1225 55\n" +
                "1330\n" +
                "1405 20 40\n" +
                "1500 15 30\n" +
                "1610 30\n" +
                "1725\n" +
                "1825\n" +
                "1920 40\n" +
                "2015\n" +
                "2100 30\n" +
                "2215 45\n" +
                "2320\n" + "Smer B: Polasci iz BUKOVAC\n" + "0405 50\n" +
                "0510 30 50\n" +
                "0610 40\n" +
                "0705 30 55\n" +
                "0830\n" +
                "0950\n" +
                "1110 50\n" +
                "1235\n" +
                "1305 35\n" +
                "1410 50\n" +
                "1525 40\n" +
                "1600 10 55\n" +
                "1715\n" +
                "1810\n" +
                "1910\n" +
                "2000 20\n" +
                "2100 40\n" +
                "2210 55\n" +
                "2320 55CF");
        lokacijeDeskripcijeMap.put("68.SR.KAMENICA - VOJINOVO", "Smer A: Polasci za SR.KAMENICA - VOJINOVO\n" + "0620IS\n" +
                "0710IS\n" +
                "1050\n" +
                "1240IS\n" +
                "1330IS\n" +
                "1620\n" + "Smer B: Polasci iz VOJINOVO - SR.KAMENICA\n" + "0405\n" +
                "0710KP 45\n" +
                "1100 30\n" +
                "1315IS\n" +
                "1410IS\n" +
                "1655");
        lokacijeDeskripcijeMap.put("69.SR.KAMENICA - CARDAK", "Smer A: Polasci za SR.KAMENICA - ČARDAK\n" + "0605 35\n" +
                "0725 55\n" +
                "0820\n" +
                "1010\n" +
                "1120\n" +
                "1225\n" +
                "1330\n" +
                "1415 45\n" +
                "1535\n" +
                "1655\n" +
                "1800\n" +
                "1905\n" +
                "2000\n" +
                "2230\n" + "Smer B: Polasci iz ČARDAK - SR.KAMENICA\n" + "0500 50\n" +
                "0650\n" +
                "0710 25\n" +
                "0810 35\n" +
                "0900\n" +
                "1050\n" +
                "1200\n" +
                "1300 40DJS\n" +
                "1410\n" +
                "1500 30\n" +
                "1615\n" +
                "1730\n" +
                "1840\n" +
                "1940\n" +
                "2040");
        lokacijeDeskripcijeMap.put( "71.SR.KAMENICA- BOCKE", "Smer A: Polasci za SR.KAMENICA - BOCKE\n" + "0540IS\n" +
                "0610IS 40IS 45IS\n" +
                "0700IS 25IS\n" +
                "0810\n" +
                "0900 15IS 55\n" +
                "1110 40\n" +
                "1220IS 50IS\n" +
                "1310IS 50\n" +
                "1400IS 40\n" +
                "1500IS 45\n" +
                "1600 50\n" +
                "1715\n" +
                "1805IS 50Č\n" +
                "2035\n" +
                "2105VMC 50\n" +
                "2210\n" +
                "2325Č\n" +
                "0000\n" + "Smer B: Polasci iz BOCKE - SR.KAMENICA\n" + "0445\n" +
                "0550IS\n" +
                "0620IS 50IS\n" +
                "0720 40IS\n" +
                "0800\n" +
                "0900 40IS 55Č\n" +
                "1030\n" +
                "1150\n" +
                "1220IS\n" +
                "1300IS 30IS 50IS\n" +
                "1430IS 40\n" +
                "1520IS 40\n" +
                "1620 35\n" +
                "1730 55\n" +
                "1845IS\n" +
                "1930IS\n" +
                "2110 45\n" +
                "2225 50\n" +
                "0000KPO 30");
        lokacijeDeskripcijeMap.put( "72.PARAGOVO", "Smer A: Polasci za PARAGOVO\n" + "0530\n" +
                "0600 50IS\n" +
                "0740\n" +
                "0830\n" +
                "0930\n" +
                "1035IS\n" +
                "1130IS 50\n" +
                "1230\n" +
                "1320\n" +
                "1420\n" +
                "1515\n" +
                "1610\n" +
                "1700 40\n" +
                "1810\n" +
                "1900 35\n" +
                "2010 50\n" +
                "2135\n" +
                "2240\n" + "Smer B: Polasci iz PARAGOVO\n" + "0500\n" +
                "0610 40\n" +
                "0720 30\n" +
                "0820\n" +
                "0910\n" +
                "1010\n" +
                "1120\n" +
                "1210 30IS\n" +
                "1310 25CDJ\n" +
                "1400\n" +
                "1500 55\n" +
                "1650\n" +
                "1740\n" +
                "1820 50\n" +
                "1940\n" +
                "2015 50\n" +
                "2130\n" +
                "2215\n" +
                "2320");
        lokacijeDeskripcijeMap.put("73.MOSINA VILA", "Smer A: Polasci za MOŠINA VILA\n" + "0720\n" +
                "0940\n" +
                "1100\n" +
                "1200IS\n" +
                "1340\n" +
                "1410IS\n" +
                "1510\n" + "Smer B: Polasci iz MOŠINA VILA\n" + "0530\n" +
                "0800\n" +
                "1020\n" +
                "1140IS\n" +
                "1235\n" +
                "1420\n" +
                "1500IS 45");
        lokacijeDeskripcijeMap.put("74.POPOVICA", "Smer A: Polasci za POPOVICA\n" + "0510 50IS\n" +
                "0630\n" +
                "0755\n" +
                "0850\n" +
                "1015\n" +
                "1115\n" +
                "1205\n" +
                "1300\n" +
                "1430\n" +
                "1530\n" +
                "1635\n" +
                "1725\n" +
                "1835\n" +
                "1915\n" +
                "2000\n" +
                "2120\n" +
                "2225\n" + "Smer B: Polasci iz POPOVICA\n" + "0500 50\n" +
                "0625\n" +
                "0710\n" +
                "0840\n" +
                "0935\n" +
                "1100\n" +
                "1200 50\n" +
                "1345\n" +
                "1515\n" +
                "1615\n" +
                "1720\n" +
                "1810\n" +
                "1915 55\n" +
                "2040\n" +
                "2200\n" +
                "2305");
        lokacijeDeskripcijeMap.put("76.S.LEDINCI", "Smer A: Polasci za S.LEDINCI\n" + "0455 55TLB\n" +
                "0610\n" +
                "0705NL 30NL\n" +
                "0825\n" +
                "0925NL\n" +
                "1025NL\n" +
                "1115NL 55NL\n" +
                "1245NL\n" +
                "1325NL 55NL\n" +
                "1425NL 50NL\n" +
                "1525NL\n" +
                "1625NL\n" +
                "1735NL\n" +
                "1840NL\n" +
                "1935NL\n" +
                "2025NL\n" +
                "2130NL\n" +
                "2245NL\n" + "Smer B: Polasci iz S.LEDINCI\n" + "0500NL 30NL\n" +
                "0600NL 20NL 45NL\n" +
                "0720NL 45NL\n" +
                "0815NL\n" +
                "0905NL\n" +
                "1005NL\n" +
                "1115NL\n" +
                "1200NL 40NL\n" +
                "1330NL\n" +
                "1410NL 40\n" +
                "1510 35NL\n" +
                "1610NL\n" +
                "1710\n" +
                "1820NL\n" +
                "1925NL\n" +
                "2020\n" +
                "2110\n" +
                "2210\n" +
                "2320");
        lokacijeDeskripcijeMap.put("77.STARI RAKOVAC", "Smer A: Polasci za STARI RAKOVAC\n" + "0600\n" +
                "1145\n" +
                "1445\n" +
                "1615\n" + "Smer B: Polasci iz STARI RAKOVAC\n" + "0545\n" +
                "0640\n" +
                "1230\n" +
                "1525\n" +
                "1700");
        lokacijeDeskripcijeMap.put("78.BEOCIN SELO", "Smer A: Polasci za BEOČIN SELO\n" + "1035SR\n" +
                "1210SR\n" +
                "1320\n" +
                "1420\n" +
                "1510\n" +
                "1600\n" +
                "1700SR 25\n" +
                "1830SR\n" +
                "1940SR\n" + "Smer B: Polasci iz BEOČIN SELO\n" + "0435SR\n" +
                "0610\n" +
                "1130\n" +
                "1305SR\n" +
                "1510\n" +
                "1600 50\n" +
                "1755\n" +
                "1815\n" +
                "1930\n" +
                "2040");
        lokacijeDeskripcijeMap.put( "79. CEREVIC", "Smer A: Polasci za ČEREVIĆ\n" + "0435\n" +
                "0510BS 55\n" +
                "0700SR 50\n" +
                "0830\n" +
                "1020\n" +
                "1115\n" +
                "1240BS\n" +
                "1325\n" +
                "1435\n" +
                "1535\n" +
                "1630BS\n" +
                "1730Đ\n" +
                "1805BS\n" +
                "1910\n" +
                "2030BS\n" +
                "2245BS\n" + "Smer B: Polasci iz ČEREVIĆ\n" + "0420BS 55\n" +
                "0535\n" +
                "0620BS 50\n" +
                "0720ĐBS\n" +
                "0805 50\n" +
                "0925BS\n" +
                "1120\n" +
                "1215\n" +
                "1340\n" +
                "1425\n" +
                "1535\n" +
                "1635\n" +
                "1730\n" +
                "1830Đ\n" +
                "1905\n" +
                "2010\n" +
                "2215\n" +
                "2340DB");
        lokacijeDeskripcijeMap.put( "80.B.A.S.", "Smer A: Polasci za B.A.S.\n" + "1900\n" +
                "0100\n" + "Smer B: Polasci iz B.A.S.\n"  + "0625Đ\n" +
                "0700Đ\n" +
                "1230Đ\n" +
                "1945\n" +
                "0130");
        lokacijeDeskripcijeMap.put( "81.BANOSTOR", "Smer A: Polasci za BANOŠTOR\n" + "0615\n" +
                "0910\n" +
                "1135\n" +
                "1545\n" +
                "1840\n" + "Smer B: Polasci iz BANOŠTOR\n" + "0605\n" +
                "0720DN\n" +
                "1020\n" +
                "1235DN\n" +
                "1650\n" +
                "1950");
        lokacijeDeskripcijeMap.put( "84.LUG", "Smer A: Polasci za LUG\n" + "0540GR\n" +
                "0955GR\n" +
                "1140\n" +
                "1250DN\n" +
                "1335\n" +
                "1440SV\n" +
                "1735\n" +
                "1950GR\n" +
                "2135\n" + "Smer B: Polasci iz LUG\n" + "0500SV 45\n" +
                "0715\n" +
                "1135\n" +
                "1255GR\n" +
                "1415SV\n" +
                "1500SV\n" +
                "1615\n" +
                "1900SV\n" +
                "2115DB\n" +
                "2250DB");
        lokacijeDeskripcijeMap.put( "86.VRDNIK", "Smer A: Polasci za VRDNIK\n" + "0510\n" +
                "0645\n" +
                "0930\n" +
                "1110\n" +
                "1250\n" +
                "1400\n" +
                "1510\n" +
                "1630\n" +
                "1740\n" +
                "2015\n" +
                "2210\n" + "Smer B: Polasci iz VRDNIK\n" + "0615\n" +
                "0750\n" +
                "1035\n" +
                "1215\n" +
                "1355\n" +
                "1505\n" +
                "1615\n" +
                "1735\n" +
                "1845\n" +
                "2120\n" +
                "2315");








        LinearLayout mainLayout= findViewById(R.id.scrollablelayout);

        Intent intent = getIntent();
        if (intent.hasExtra("lokacija")){
            String lokacija = intent.getStringExtra("lokacija");
            String deskripcija = lokacijeDeskripcijeMap.get(lokacija);

            TextView textView = findViewById(R.id.textView);
            textView.setText(lokacija);

            TextView textViewDeskripcija = findViewById(R.id.textViewDeskripcija);
            textViewDeskripcija.setText(deskripcija);

        }


        for(int i = 0; i <lokacije.length;i++){
            LayoutInflater inflater = getLayoutInflater();
            View red = inflater.inflate(R.layout.jedanred,null);

            Button button6 = red.findViewById(R.id.button6);
            button6.setText(lokacije[i]);

            final int index = i;

            DBHelper2 dbHelper2 = new DBHelper2(this);

            button6.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String selectedLocation = lokacije[index];
                    String selectedDescription = lokacijeDeskripcijeMap.get (selectedLocation);
                    dbHelper2.insertSelectedLocation(selectedLocation, selectedDescription);
                    Intent newIntent = new Intent(getApplicationContext(),RedVoznje.class);

                    newIntent.putExtra("lokacija", selectedLocation);
                    newIntent.putExtra("deskripcija", selectedDescription);

                    startActivity(newIntent);
                }
            });








            mainLayout.addView(red);

        }
    }
}