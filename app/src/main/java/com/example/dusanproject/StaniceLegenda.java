package com.example.dusanproject;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StaniceLegenda extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stanice_legenda);
        TextView textView = findViewById(R.id.textView3);


        String originalText = "GL=GORNJE LIVADE,\n" +
                "        NRG=NISKOPODNI SA RAMPOM GORNJE LIVADE,\n" +
                "         NRJ=NISKOPODNI SA RAMPOM JEGRICKA,\n" +
                "         D=DO DANUBIUSA PRISTANISNA ZONA,\n" +
                "          NĐ=DO NOVITETA ĐAČKI,\n" +
                "           NRD=NISKOPODNI SA RAMPOM DO DANUBIUSA,\n" +
                "           ALI=ALIBEGOVAC,AL=ALBUS,\n" +
                "            ALM=ALBUS I MASINOREMONT,\n" +
                "             APT=APTIV SA I DO Z.STANICE,\n" +
                "              MAL=MASINOREMONT I ALBUS,\n" +
                "              MDJ=DO MEDICINSKE SKOLE DJACKI,\n" +
                "              UDJ=BUL.EVROPE K.T-UNIVERZITET DJACKI,\n" +
                "DDJ=DO DETELINARE DJACKI,BM=BMTS Z.SEVER IV DO I OD Z.STANICE,\n" +
                " KN=OD KOTEKSPROD.PORED NEOPLANTE,\n" +
                " KVL=KOTEKS VISKOFAN OD I DO LIDLA,\n" +
                " NK=DO NEOPLANTE I KOTEKSPRODUKTA,\n" +
                "  NP=NEOPLANTA\n" +
                ",NIS=PARKING NIS,\n" +
                " TET=TERMOELEKTRANA TOPLANA,C=DO CENTRA N.SADA,\n" +
                "  Đ=NE SAOBRA.ZA VREME ĐAČKOG RASPUSTA,\n" +
                "  KC=KROZ CENTAR KAĆA,\n" +
                "NT=NOVOM TRASOM KROZ KAC,\n" +
                "AKN=OD ALBUSA KOTEKSPRO.I NEOPLANTE,\n" +
                "KSA=OD KISAČA DO AUTOKAROSERIJE,\n" +
                "ĐK=ĐAČKI KOLONIJA,\n" +
                "K=KOLONIJA TEMERIN,\n" +
                "ML=PORED MLEKARE\n" +
                "DT=DO TEMERINA,\n" +
                " T=KROZ TELEP U TEMERINU,\n" +
                "ČL=KROZ LIS,\n" +
                " ČLĐ=KROZ LIS DJACKI,\n" +
                "KR=KROZ RUMENKU,\n" +
                " RTK=KROZ RUMENKU I TANK.UL. U KISACU,\n" +
                "  TK=KROZ TANKOS.UL.U KISAČU,\n" +
                "SP=SAOBRACA SAMO PETKOM,\n" +
                "CG=CENTRALNO GROBLJE,\n" +
                "A=AROMA-NE SAOBRAĆA GRMEČKOM,\n" +
                " AZG=OD AROME ZELEZNIC.GRMEC.DO NS,\n" +
                "KF=OD KANALA NOVIM FUTOGOM,\n" +
                "ZA=I ZELEZNICKOM DO AROME,\n" +
                "SF=STARI FUTOG,\n" +
                "CĐ=KROZ CENTAR DJACKI,\n" +
                "CKR=KROZ CENTAR KARLOVACA,\n" +
                " DJC=DO CENTRA KARLOVACA DJACKI,\n" +
                "KD=KROZ DUDARU,\n" +
                " OC=OD CEN.KARL.KROZ CENTAR N.S.NA MAS,\n" +
                " KVG=KROZ VINOGRADARSKU,\n" +
                "CF=KROZ CENTAR DO FUTOSKOG STAJALISTA,\n" +
                "IS=KROZ INSTITUT,\n" +
                "KP=KROZ PARAGOVO,\n" +
                "DJS=DJACKI PREKO MOSTA SLOBODE,\n" +
                "Č=KROZ ČARDAK,\n" +
                "KPO=KROZ POPOVICU,\n" +
                " VMC=VARADINSKIM MOSTOM KROZ CARDAK,\n" +
                "CDJ=KROZ CARDAK DJACKI,\n" +
                "NL=KROZ N.LEDINCE,\n" +
                "TLB=TRASOM LIN.BROJ 71 BOCKE,\n" +
                "SR=KROZ STARI RAKOVAC,\n" +
                "BS=KROZ BEOČIN SELO,\n" +
                " DB=DO B.A.S.-A,\n" +
                "ĐBS=ĐAČKI KROZ B.SELO,\n" +
                "DN=KROZ DUNAV NASELJE DJACKI,\n" +
                "GR=KROZ GRABOVO,\n" +
                "SV=KROZ SVILOŠ";


        Pattern pattern = Pattern.compile("\\b\\w{1,3}\\b");
        Matcher matcher = pattern.matcher(originalText);


        SpannableString spannableString = new SpannableString(originalText);

        while (matcher.find()) {
            int startIndex = matcher.start();
            int endIndex = matcher.end();


                spannableString.setSpan(new StyleSpan(Typeface.BOLD), startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }



        textView.setText(spannableString);
    }}


