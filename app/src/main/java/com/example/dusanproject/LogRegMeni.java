package com.example.dusanproject;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

public class LogRegMeni extends AppCompatActivity {

    EditText username,password,repassword;
    Button buttonregister,buttonlogin;

    DBHelper DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_reg_meni);

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        repassword = (EditText) findViewById(R.id.repassword);
        buttonregister = (Button) findViewById(R.id.buttonregister);
        buttonlogin = (Button) findViewById(R.id.buttonlogin);
        DB = new DBHelper(this);

        buttonregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = username.getText().toString();
                String pass = password.getText().toString();
                String repass = repassword.getText().toString();

                if(user.equals("")||pass.equals("")||repass.equals(""))
                Toast.makeText(LogRegMeni.this, "Molim vas popunite sva polja",Toast.LENGTH_SHORT).show();
                else{
                    if(pass.equals(repass)){
                        Boolean checkuser = DB.checkusername(user);
                        if(checkuser == false){
                            Boolean insert = DB.insertData(user,pass);
                            if (insert==true){
                                Toast.makeText(LogRegMeni.this ,"Uspesno registrovan",Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent (getApplicationContext(),MainMeni.class);
                                startActivity(intent);
                            }else{
                                Toast.makeText(LogRegMeni.this, "Registracija neuspesna", Toast.LENGTH_SHORT).show();
                            }

                        }
                        else{
                            Toast.makeText(LogRegMeni.this,"Korisnik vec postoji, probajte ponovo!",Toast.LENGTH_SHORT).show();
                        }
                    }
                }


            }
        });

        buttonlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (getApplicationContext(),LoginMeni.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.imageButton2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent (getApplicationContext(),PocetniMeni.class);
                startActivity(i);

            }
        });



    }
}