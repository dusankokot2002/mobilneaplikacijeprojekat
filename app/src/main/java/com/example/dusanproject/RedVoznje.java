package com.example.dusanproject;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Map;

public class RedVoznje extends AppCompatActivity {

    private LokacijeSingleton lokacijeSingleton;

    private DBHelper2 dbHelper2;

    private void loadSavedData() {
        DBHelper2 dbHelper2 = new DBHelper2(this);
        Cursor cursor = dbHelper2.getSelectedLocation();

        while (cursor.moveToNext()) {
            @SuppressLint("Range") String lokacija = cursor.getString(cursor.getColumnIndex("location"));
            @SuppressLint("Range") String deskripcija = cursor.getString(cursor.getColumnIndex("description"));

            if (!lokacijeSingleton.sadrziLokaciju(lokacija)) {
                lokacijeSingleton.dodajLokacijuIDeskripciju(lokacija, deskripcija);
            }
        }

        cursor.close();
        updateLayout();
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_red_voznje);


        if (lokacijeSingleton == null){
            lokacijeSingleton = LokacijeSingleton.getInstance();
        }

        dbHelper2 = new DBHelper2(this);

        loadSavedData();

        if (getIntent().hasExtra("lokacija")) {
            String lokacija = getIntent().getStringExtra("lokacija");
            String deskripcija = getIntent().getStringExtra("deskripcija");



            if (!lokacijeSingleton.sadrziLokaciju(lokacija)) {
                lokacijeSingleton.dodajLokacijuIDeskripciju(lokacija, deskripcija);


                updateLayout();
            }
        }

        findViewById(R.id.addbutton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), BusStanice.class);
                startActivity(i);

                saveData();
            }
        });
        findViewById(R.id.backbutton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (getApplicationContext(),MainMeni.class);
                startActivity(intent);
            }
        });
    }

    private void saveData() {
        for (Map.Entry<String, String> entry : lokacijeSingleton.getLokacijeDeskripcijeMap().entrySet()) {
            String lokacija = entry.getKey();
            String deskripcija = entry.getValue();

            boolean success = dbHelper2.insertSelectedLocation(lokacija, deskripcija);

            if (success) {
                Toast.makeText(RedVoznje.this, "Podaci sačuvani.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(RedVoznje.this, "Greška prilikom čuvanja podataka.", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void updateLayout() {
        LinearLayout containerLayout = findViewById(R.id.containerLokacije);


        containerLayout.removeAllViews();


        for (Map.Entry<String, String> entry : lokacijeSingleton.getLokacijeDeskripcijeMap().entrySet()) {
            String lokacija = entry.getKey();
            String deskripcija = entry.getValue();

            TextView textViewLokacija = new TextView(this);
            textViewLokacija.setText(lokacija);

            TextView textViewDeskripcija = new TextView(this);
            textViewDeskripcija.setText(deskripcija);


            containerLayout.addView(textViewLokacija);
            containerLayout.addView(textViewDeskripcija);

            Button deleteButton = new Button(this);
            deleteButton.setText("Obriši");
            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    lokacijeSingleton.obrisiLokaciju(lokacija);
                    dbHelper2.obrisiLokaciju(lokacija);
                    updateLayout();
                }
            });
            containerLayout.addView(deleteButton);
        }
    }}