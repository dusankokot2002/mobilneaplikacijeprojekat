package com.example.dusanproject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper2 extends SQLiteOpenHelper {
    public static final String DBNAME = "SelectedLocations.db";

    public DBHelper2(Context context) {
        super(context, "SelectedLocations.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase MyDB) {
        MyDB.execSQL("CREATE TABLE selected_locations (id INTEGER PRIMARY KEY AUTOINCREMENT, location TEXT, description TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase MyDB, int i, int i1) {
        MyDB.execSQL("DROP TABLE IF EXISTS selected_locations");
        onCreate(MyDB);
    }

    public boolean insertSelectedLocation(String lokacija, String deskripcija) {
        SQLiteDatabase MyDB = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("location", lokacija);
        contentValues.put("description", deskripcija);
        long result = MyDB.insert("selected_locations", null, contentValues);
        return result != -1;
    }

    public Cursor getSelectedLocation() {
        SQLiteDatabase MyDB = this.getWritableDatabase();
        return MyDB.query("selected_locations", null, null, null, null, null, null);
    }
    public void obrisiLokaciju(String lokacija) {
        SQLiteDatabase MyDB = this.getWritableDatabase();
        MyDB.delete("selected_locations", "location = ?", new String[]{lokacija});
    }
}