package com.example.dusanproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginMeni extends AppCompatActivity {

    EditText username,password;
    Button buttonlogin;
    DBHelper DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_meni);

        username = (EditText) findViewById(R.id.username1);
        password = (EditText) findViewById(R.id.password1);
        buttonlogin = (Button) findViewById(R.id.buttonlogin1);
        DB = new DBHelper(this);

        buttonlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = username.getText().toString();
                String pass = password.getText().toString();

                if(user.equals("")||pass.equals(""))
                    Toast.makeText(LoginMeni.this,"Molim vas popunite sva polja",Toast.LENGTH_SHORT).show();
                else{
                    Boolean checkuserpass = DB.checkusernamepassword(user,pass);
                    if(checkuserpass==true){
                        Toast.makeText(LoginMeni.this,"Logovanje uspesno",Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent (getApplicationContext(),MainMeni.class);
                        startActivity(intent);
                    }else{
                        Toast.makeText(LoginMeni.this,"Nepostojeci korisnik",Toast.LENGTH_SHORT).show();
                    }
                }




            }
        });

        findViewById(R.id.imageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent (getApplicationContext(),LogRegMeni.class);
                startActivity(i);

            }
        });
    }
}